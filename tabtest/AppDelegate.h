//
//  AppDelegate.h
//  tabtest
//
//  Created by James Cash on 18-01-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

